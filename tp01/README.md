# TP0X - Titre du TP

# Progression et remarque
Petit problème de tabulation pour le makefile plus détaillé en dessoous

# Utilisation
Vous disposez d'un Makefile avec plusieurs règles :

Taper la commande suivante permet de compiler les fichiers nécessaire à générer le programme de l'exercice 1 :

    make ex1

Lors de l'execution du programme, plusieurs options peuvent être passé au programme :

* `-o` : permet de ...

#TP1

#Q1.
a.out

#Q3.
La fonction_b n'est pas prise en compte car il y a une erreur dans la comparaison dans le if.

#Q4.
Le compilateur considère if(param = 0) comme un assignement de valeur alors que c'est censé être une comparaison entre la valeur 0 et param.
Il faut donc faire if(param == 0)

#Q6.
Chacune des erreurs est une implicit-fonction-decaration ce qui veux dire que les fonctions n'ont jamais été spécifié dans la classe. Pour régler le problème il faut les déclarer.

#Q10.
Les tabulations ne se mettant pas on a trouvé une commande sur internet qui permet de detecter les tabulations et de remplacer les "fausses" tabulations par des vraies.

cat -e -t -v Makefile
perl -pi -e 's/^  */\t/' Makefile
