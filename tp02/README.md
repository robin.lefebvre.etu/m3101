# TP02 - Tableaux et structures

# Progression et remarque
Tout a été fait et fonctionne correctement. 

## Remarque 1
Juste une petite galère sur la dernière question pour trouver la manière pour comparer deux fractions entres elles.

# Utilisation
Vous disposez d'un Makefile avec plusieurs règles :

Taper la commande suivante permet de compiler le programme répondant à l'exercice 1 :

    make

Lors de l'execution du programme, plusieurs options peuvent être passé au programme :

`clean` : permet de retirer les -o

`realclean`: permet de retirer les -o, les fichiers cachés et l'executable.


