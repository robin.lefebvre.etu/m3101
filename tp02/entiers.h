#ifndef ENTIERS_H
#define ENTIERS_H
void afficher(int liste[], int taille);
int somme(int liste[], int taille);
void copie_dans(int dest[], int src[], int taille);
void ajoute_apres(int dest[], int taille_dest, int src[], int taille_src);
#endif