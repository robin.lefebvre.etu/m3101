#include <stdio.h>

struct rat
{
    int den;
    int num;
};

struct rat rat_produit(struct rat n1, struct rat n2){
    struct rat stock = {n1.den*n2.den,n1.num*n2.num};
    return stock;
}

struct rat rat_somme(struct rat n1, struct rat n2){
    struct rat stock;
    if(n1.den == n2.den){
        stock.den = n1.den;
        stock.num = n1.num + n2.num;
    }else{
        stock.den = n1.den*n2.den;
        stock.num = (n1.num*n2.den) + (n2.num*n1.den);
    }
    return stock;
} 

struct rat rat_plus_petit(struct rat list[]){
    struct rat res = list[0];
    int cmpt = 0;
    while(list[cmpt].den != 0){
        if(list[cmpt].den!=0 && (res.num*list[cmpt].den)-(res.den*list[cmpt].num) > 0){
            res.den = list[cmpt].den;
            res.num = list[cmpt].num;
        }
        cmpt++;
    }
    if(res.den == 0){
        res.num = 1;
    }
    return res;
}