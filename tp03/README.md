# TP03 - Chaînes de caractères

# Progression et remarque
J'ai tous réussis les questions du tp mais avec certaines difficultés puisque je ne me souvenais plus bien des pointeurs.

## Remarque 1
NE PAS PRENDRE LA TAILLE D'UN TABLEAU DE MANIÈRE DYNAMIQUE.

# Utilisation
Vous disposez d'un Makefile avec plusieurs règles :

Taper la commande suivante permet de compiler le programme répondant à l'exercice:

    make

Lors de l'execution du programme, plusieurs options peuvent être passé au programme :

* `clean` : permet de retirer les -o
* `realclean`: permet de retirer les -o, les fichiers cachés et l'executable.


