#include <stdio.h>

int mon_strlen_tab(char s[]){
	int res = 0;
	while(s[res]!='\0'){
		res++;
	}
	return res;
}

int mon_strlen(const char *s){
	int res = 0;
	while(*(s+res)!='\0'){
		res++;
	}
	return res;
}

int mon_strcmp(const char * s1, const char * s2){
	int cmpt = 0;
	int l_s1 = mon_strlen(s1);
	int l_s2 = mon_strlen(s2);

	while(l_s1>=cmpt && l_s2>=cmpt){
		if(*(s1+cmpt)<*(s2+cmpt)){
			return -1;
		}else if(*(s1+cmpt)>*(s2+cmpt)){
			return 1;
		}
		cmpt++;
	}
	return 0;
}

int mon_strncmp(const char * s1, const char * s2, int n){
	int cmpt = 0;
	int l_s1 = mon_strlen(s1);
	int l_s2 = mon_strlen(s2);
	while(cmpt<n && l_s1>=cmpt && l_s2>=cmpt){
		if(*(s1+cmpt)<*(s2+cmpt)){
			return -1;
		}else if(*(s1+cmpt)>*(s2+cmpt)){
			return 1;
		}
		cmpt++;
	}
	return 0;
}

char *mon_strcat(char *s1, const char *s2){
	int l_s1 = mon_strlen(s1);
	int l_s2 = mon_strlen(s2);
	for(int i = 0;i<l_s2;i++){
		*(s1+l_s1 +i) = *(s2+i);
		if(i==l_s2-1){
			*(s1+l_s1 +i+1) = '\0';
		}
	} 
	return s1;
}

char *mon_strchr(const char *s, int c){
	char *res = NULL;
	int l_s = mon_strlen(s);
	for(int i  = 0; i<l_s;i++){
		if(*(s+i)==c){
			res = (char*)(s+i);
			return res;
		}
	} 
	return res;
}

//Q7
/*char *mon_strstr(const char *haystack, const char *needle){
	char* res = NULL;
	int l_needle = mon_strlen(needle);
	int l_haystack = mon_strlen(haystack);
	if(l_needle==0){
		return (char*)(haystack);
	}
	for(int i  = 0; i<l_haystack ;i++){
		if(*(haystack+i)==*(needle)){
			int k = 0; 
			while(k<l_needle && *(haystack+k+i) == *(needle+k)){
				if(k==l_needle-1){
					return (char*)(haystack+i);
				}
				k++;
			}
		}
	} 		
	return res;
}*/
//Q8
char *mon_strstr(const char *haystack, const char *needle){
	char* res = NULL;
	int l_needle = mon_strlen(needle);
	int l_haystack = mon_strlen(haystack);
	if(l_needle==0){
		return (char*)(haystack);
	}
	for(int i  = 0; i<l_haystack ;i++){
		if(mon_strncmp(haystack+i, needle,l_needle)==0){
			return (char*)(haystack+i);
		}
	} 		
	return res;
}
//mon_strncmp(const char * s1, const char * s2, int n)