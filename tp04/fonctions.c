#include "fonctions.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "tests.h"


char * miroir(const char *s){
	int cmpt = strlen(s);
	char* res = malloc((cmpt+1)*sizeof(char));
	
	for (int i = 0; i < cmpt; i++) {
		res[i] = s[cmpt - i - 1];
	}
	res[cmpt] = '\0';
	return res;
}


char * saisie(){
	int cmptMal = 0;
	int max = 100000;
	char * res = malloc(max *sizeof(char));
	char c = getchar();

	while(c!= EOF && c!='\n' && isspace(c) == 0){
		cmptMal++;
		if(cmptMal%max==0){
            res = realloc(res, max + cmptMal);
		}
		res[cmptMal-1]=c;
		c = getchar();
	}
	if(res == NULL){
		free(res);
		return NULL;
	}
    res = realloc(res, cmptMal+1);
	res[cmptMal]='\0';
	return res;
}