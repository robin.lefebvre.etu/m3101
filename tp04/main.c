#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fonctions.h"

int main(int nbarg,char ** opt)
{
	int d = 0;
	char * ch = NULL;
	char * mir = NULL;
	for(int i = 1 ; nbarg >= 2 && i<nbarg;i++){
		if(*(*(opt+i))=='-'){
			if(strchr(*(opt+i),'s')!=NULL && strchr(*(opt+i),'m')==NULL){
				if(d==1 || d==3){
					d=3;
				}else{
					d=2;
				}
				
			}else if(strchr(*(opt+i),'m')!=NULL && strchr(*(opt+i),'s')==NULL){
				if(d==2 || d==3){
					d=3;
				}else{
					d=1;
				}
			}else if(strchr(*(opt+i),'m')!=NULL && strchr(*(opt+i),'s')!=NULL){
				d=3;
			}else{
				printf("Mauvaise utilisation\n");
				return -1;
			}
		}
		if(*(*(opt+i))!='-' && d!=0){
			ch = *(opt+i);
		}else if(*(*(opt+i))!='-' && d==0){
				printf("Mauvaise utilisation\n");
				return -1;
		}
	}
	if(nbarg < 2){
		printf("Mauvaise utilisation\n");
		return -1;
	}
	if(ch!=NULL && d==1){
		mir = miroir(ch);
		printf("%s\n",mir);
		free(mir);
	}else if(ch==NULL && d==1){
		printf("Mauvaise utilisation\n");
		return -1;
	}
	if(ch==NULL && d==2){
		ch = saisie();
		printf("%s\n",ch);
		free(ch);
		free(mir);
	}else if(ch!=NULL && d==2){
		printf("Mauvaise utilisation\n");
		return -1;

	}
	if(ch==NULL && d==3){
		ch = saisie();
		mir = miroir(ch);
		printf("%s\n",mir);
		free(ch);
		free(mir);
	}else if(ch!=NULL && d==3){
		printf("Mauvaise utilisation\n");
		return -1;
	}
	
/*
printf("%d,%s\n",nbarg,*(opt));
char * res = miroir("Bonjour");
printf("oh\n");
printf("%s\n",res);
res = saisie();
printf("ah\n");
printf("%s\n",res);
res = saisie();
printf("ah\n");
printf("%s\n",res);
res = saisie();
printf("ah\n");
printf("%s\n",res);
res = saisie();
printf("ah\n");
printf("%s\n",res);
free(res);
return 0;*/
}