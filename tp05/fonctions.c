#include "fonctions.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int error(){
    fprintf(stderr, "\nMAUVAISE UTILISATION DU WORDCOUNT\n\nExemple :\n./tp05 -c -m -l exemple\n");
    return 0;
}

int traiter (int f, int *car, int *mot, int *lig){
    const int maxBuff = 80;
    char buffer[maxBuff]; 
    int no = read(f,buffer,maxBuff);
    int d = -1;
    if(no == -1) return -1;
    while(no > 0 ){
        for(int i = 0; i<no; i++){
            car[0]++;
            
            if((isspace(buffer[i])==0) && d==-1){
                d = 1;
            }
            if((isspace(buffer[i])!=0) && d==1){
                d =-1;
                mot[0]++;
            }

            if(buffer[i]=='\n'){
                lig[0]++;
            }
        }
        if(f == 0 && no < maxBuff){
            no=0;
        }else{
            no = read(f,buffer,maxBuff); 
        }
    }
    if(d == 1){
        d =-1;
        mot[0]++; 
    }
    return 1;
}