#include "fonctions.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


int main(int argc, char **argv)
{
    int charTotal = 0;
    int motTotal = 0;
    int ligneTotal = 0;
    int carOK = 0;
    int motOK = 0;
    int ligneOK = 0;
    int nbPath = 0;
    int car = 0;
    int mot = 0;
    int ligne = 0;
    int file = 0;


    char path[500][500];

    int i = 1;
    int stop = 0;
    while(argv[i]!=NULL && stop == 0 && argc>1){
        if(argv[i][0]=='-'){
            if(argv[i][1]=='c'){
                carOK = 1;
                i++;
            }else if(argv[i][1]=='m'){
                motOK = 1;
                i++;
            }else if(argv[i][1]=='l'){
                ligneOK = 1;
                i++;
            }else{
                return error();
            }
        }else{
            stop = 1;
        }
    }
    while(argv[i]!=NULL){
        if(argv[i][0]!='-'){
            memcpy(path[nbPath], argv[i], 50);
            nbPath++;
        }
        i++;
    }

    if(nbPath==0){
        traiter (0, &car, &mot, &ligne);
        if(carOK==1){
            printf("NB car = %d\n", car); 
            charTotal+=car;
        } 
        if(motOK==1){
            printf("NB mot = %d\n", mot);
            motTotal+=mot;
        } 
        if(ligneOK==1){
            printf("NB ligne = %d\n", ligne); 
            ligneTotal+=ligne;
        } 
		if(carOK==0 && motOK==0 && ligneOK==0){
			printf("NB car = %d\n", car);
			printf("NB mot = %d\n", mot);
			printf("NB ligne = %d\n", ligne);
            ligneTotal+=ligne;
            charTotal+=car;
            motTotal+=mot;
		}
        return 0;
    }

    for(int i = 0; i<nbPath; i++){
        car = 0;
        mot = 0;
        ligne = 0;
        int check = open(path[i], O_RDONLY);
        
		if(check==-1){
			error();
		}

        if(traiter(check, &car, &mot, &ligne)!=-1 && check!=-1){
        file++;
        printf("%s:\n", path[i]);
        if(carOK==1){
            printf("NB car = %d\n", car); 
            charTotal+=car;
        } 
        if(motOK==1){
            printf("NB mot = %d\n", mot);
            motTotal+=mot;
        } 
        if(ligneOK==1){
            printf("NB ligne = %d\n", ligne); 
            ligneTotal+=ligne;
        } 
		if(carOK==0 && motOK==0 && ligneOK==0){
			printf("NB car = %d\n", car);
			printf("NB mot = %d\n", mot);
			printf("NB ligne = %d\n", ligne);
            ligneTotal+=ligne;
            charTotal+=car;
            motTotal+=mot;
		}
        printf("\n");
        }
    }
    if(nbPath>1){
        printf("Total:\n");
        if(carOK==1) printf("NB Total car = %d\n", charTotal);
        if(motOK==1) printf("NB Total mot = %d\n", motTotal);
        if(ligneOK==1) printf("NB Total ligne = %d\n", ligneTotal);
	    if(carOK==0 && motOK==0 && ligneOK==0){
		    printf("NB Total car = %d\n", charTotal);
		    printf("NB mot = %d\n", motTotal);
		    printf("NB ligne = %d\n", ligneTotal);
        }
        printf("\n");
    }
    printf("\n%d\n",file);
    return 0;
}
