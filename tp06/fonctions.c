#include "fonctions.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

char convertir(char valeur){
	if(valeur<0 || valeur>63){
		return -1;
	}
	if(valeur<26)
		return valeur + 'A';
	else if(valeur<52)
		return valeur + 'a' - 26;
	else if(valeur<62)
		return valeur + '0' - 52;
	else if(valeur == 62)
		return '+';
	else if(valeur == 63)
		return '/';
	return -1;
}

void encoder_bloc(const char *source, int taille_source, char *destination){
	char var[3];
	var[0] = source[0];
	var[1] = 0;
	var[2] = 0;
	if(taille_source==2){
		var[1] = source[1];
	}else if(taille_source==3){
		var[1] = source[1];
		var[2] = source[2];
	}

	int D = (var[0] >> 2) & 0b00111111;
	int E = ((var[0] & 0b00000011)<<4) | ((var[1] >> 4) & 0b00001111) ;
	int F = ((var[1] & 0b00001111)<<2) | ((var[2] >> 6) & 0b00000011) ;
	int G = (var[2] & 0b00111111);

	for(int i = 0; i<4; i++){
		destination[i] = '=';
	}
	
	destination[0] = convertir(D);
	destination[1] = convertir(E);
	if(taille_source>1){
		destination[2] = convertir(F);
		if(taille_source>2){
			destination[3] = convertir(G);
		}
	}
}

int encoder_fichier(int source, int destination){
	const int maxBuff = 3;
    char buffer[maxBuff]; 
    int nSource = read(source,buffer,maxBuff);
	int nDest;
	if(nSource == -1) return -1;
	while(nSource > 0){

		char stk[4];
		encoder_bloc(buffer,nSource,stk);
		nDest = write(destination,stk,4);
		if(nDest == -1) return -1;

		
		int t = 0;
		if(source == 0){
			if(nSource<maxBuff){
				t = 1;
			}
		}

		if(t == 1){
			nSource = 0;
        }else{
			nSource = read(source,buffer,maxBuff); 
        }
	}
	return 0;
}

int encoder_fichierF(int source, int destination, int paquet){
	const int maxBuff = 3*paquet;
    char buffer[maxBuff]; 
    int nSource = read(source,buffer,maxBuff);
	int nDest;
	if(nSource == -1) return -1;
	while(nSource > 0){

		char stk[4*paquet];
		int idx = 0;
		int idx2 = 0;
		int i = 0;
		
		while(i<paquet && nSource-3*i>0){
			char stk2[4];
			char buff3[3];
			for(int j = 0; j<3 && idx2<nSource; j++){
				buff3[j] = buffer[idx2];
				idx2++;
			}
			int taille = 3;
			if(nSource-3*i>=3){
				taille = 3;
			}else {
				taille = nSource-3*i;
			}

			encoder_bloc(buff3,taille,stk2);

			for(int j = 0; j<4; j++){
				stk[idx] = stk2[j];
				idx++;
			}
			i++;
		} 
		nDest = write(destination,stk,4*i);
		if(nDest == -1) return -1;

		
		int t = 0;
		if(source == 0){
			if(nSource<maxBuff){
				t = 1;
			}
		}

		if(t == 1){
			nSource = 0;
        }else{
			nSource = read(source,buffer,maxBuff); 
        }
	}
	return 0;
}

int error(char* s){
	fprintf(stderr,"%s \n(./tp06 <source> <dest>)\n",s);
	return 0;
}