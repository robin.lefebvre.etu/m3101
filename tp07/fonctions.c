#include "fonctions.h"
#include "ligne_commande.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>

void execute_ligne_commande(){
	int flag = 0;
	int nb = 0;
	char*** tabCommande = ligne_commande(&flag, &nb);
	for(int i = 0;i<nb; i++){
		if (strcmp(tabCommande[i][0], "exit") == 0)
        {
            libere(tabCommande);
            exit(0);
        }
		// Fils
		if(fork() == 0){
			execvp(tabCommande[i][0], tabCommande[i]);

			fprintf(stderr, "execvp:%s ", tabCommande[i][0]);
			perror("");
        	exit(1);
		}

		// else // Père


		if (flag == 0) {
			wait(NULL); // Attend le fils qui se termine et le tue proprement, wait bloquant
			printf("Processus terminé !\n");
		}
		
		while (waitpid(-1, NULL, WNOHANG) > 0)
		{
			printf("Processus terminé !\n");
		}
		
	}
}

void affiche_prompt()
{
    char host[256];
    char cwd[256];
    char *utilisateur = getenv("USER");
    gethostname(host, 256);
    getcwd(cwd, 256);
    printf("\n%s@%s:%s$", utilisateur, host, cwd);
    fflush(stdout);
}